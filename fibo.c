#include <stdio.h>

int main()
{
    long szamok[100] = {1, 2};
    long sum = 2;
    int szamlal = 2;

    while(1)
    {
        szamok[szamlal] = szamok[szamlal-2] + szamok[szamlal-1];

        if(szamok[szamlal] % 2 != 0 && szamok[szamlal] < 4000000)
        {
            sum += szamok[szamlal];
        }
        else if(szamok[szamlal] >= 4000000)
        {
            break;
        }
        szamlal++;
    }

    printf("%ld \n", sum);

    return 0;
}