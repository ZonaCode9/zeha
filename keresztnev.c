#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "prog1.h"

int is_valid(string s)
{
    int hossz = strlen(s);

    for(int i = 0; i < hossz; i++)
    {
        if(isdigit(s[i]) != 0)
        {
            return 0;
        }
    }

    return 1;
}

int main(int argc, string argv[])
{
    if(argc != 2)
    {
        puts("Hiba! Pontosan egy nevet adj meg");
    }
    else if(strlen(argv[1]) < 3)
    {
        puts("Hiba! A megadott nev tul rovid!");
    }
    else if(is_valid(argv[1]) == 0)
    {
        puts("Hiba! A megadott nev helytelen karaktert tartalmaz.");
    }
    else
    {
        printf("A(z) %s nev megfelel a formai kovetelmenyeknek! \n", argv[1]);
    }

    return 0;
}