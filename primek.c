#include <stdio.h>

int is_prime( int n)
{
    if (n <= 1)
    {
        return 0;
    }
    if(n == 2)
    {
        return 1;
    }
    for(int i = 2; i * i <= n; i++)
    {
        if(n % i == 0)
        {
            return 0;
        }
    }
    return 1;
}

long nth_prime(int n)
{
    if(n == 1)
    {
        return 2;
    }
    else if(n == 2)
    {
        return 3;
    }

    int prime = 3;
    int i = 2;
    while(i < n)
    {
        do{
            prime += 2;
        } while(!is_prime(prime));
        i++;
    }
    return prime;
}

int main()
{
    printf("%ld \n", nth_prime(10001));

    return 0;
}