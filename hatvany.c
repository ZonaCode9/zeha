#include <stdio.h>
#include <stdlib.h>
#include "prog1.h"

int my_pow(int a, int b)
{
    if(a == 0 && b == 0)
    {
        return 1;
    }
    else
    {
        int eredmeny = 1;
        for(int i = 0; i < b; i++)
        {
            eredmeny *= a;
        }
        return eredmeny;
    }
}

int main(int argc, string argv[])
{
    if(argc != 3)
    {
        puts("Hibas parameterezes!");
        exit(1);
    }
    else
    {
        int a = atoi(argv[1]);
        int b = atoi(argv[2]);
        int eredmeny = my_pow(a, b);

        printf("%d ^ %d = %d \n", a, b, eredmeny);
    }


    return 0;
}