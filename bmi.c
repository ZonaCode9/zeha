#include <stdio.h>

double bmi_kalkulator(const double magassag, const double suly)
{
    double bmi = suly / (magassag * magassag);

    return bmi;
}

void tajekoztat(double bmi_ertek)
{
    if(bmi_ertek < 18.5)
    {
        puts("A testsulyozasi tablazat szerint a sovany vagy!");
    }
    else if(bmi_ertek >= 18.5 && bmi_ertek < 25)
    {
        puts("A testsulyozasi tablazat szerint normal a testsulyod!");
    }
    else if(bmi_ertek >= 25 && bmi_ertek < 30)
    {
        puts("A testsulyozasi tablazat szerint tulsulyos vagy!");
    }
    else
    {
        puts("A testsulyozasi tablazat szerint el vagy hizva!");
    }
}

int main()
{
    double magassag = 0;
    double suly = 0;

    puts("Add meg a testmagassagodat meterben!");
    scanf("%lf", &magassag);

    puts("Add meg a testsulyodat kilogrammban!");
    scanf("%lf", &suly);

    double eredmeny = bmi_kalkulator(magassag, suly);

    tajekoztat(eredmeny);

    return 0;
}