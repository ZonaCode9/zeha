#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "prog1.h"
#define SIZE 200

void statisztika()
{
    int lower_num = 0;
    int upper_num = 0;
    int digit_num = 0;
    int space_num = 0;
    char szo[SIZE];

    while (1)
    {
        printf("Szoveg (vege: *): ");
        fgets(szo, SIZE, stdin);    //valamiert ezzel jobban mukodott
        szo[strlen(szo)-1] = '\0';

        if(strcmp(szo, "*") == 0)
        {
            break;
        }

        int hossz = strlen(szo);
        for(int i = 0; i < hossz; i++)
        {
            if(islower(szo[i]))
            {
                lower_num++;
            }
            else if(isupper(szo[i]))
            {
                upper_num++;
            }
            else if(isdigit(szo[i]))
            {
                digit_num++;
            }
            else if(isspace(szo[i]))
            {
                space_num++;
            }
        }
    }

    puts("");
    printf("Kisbetuk szama: \t %d \n", lower_num);
    printf("Nagybetuk szama: \t %d \n", upper_num);
    printf("Szokozok szama: \t %d \n", space_num);
    printf("Szamjegyek szama: \t %d \n", digit_num);
}

int main()
{
    statisztika();

    return 0;
}