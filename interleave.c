#include <stdio.h>
#include "prog1.h"
#include <string.h>

void merge(string s1, string s2)
{
    int hossz = strlen(s1);
    int osszefesul_hossz = (2 * strlen(s1))+1;
    char kesz[osszefesul_hossz];

    int place = 0;

    for(int i = 0; i < osszefesul_hossz; i += 2)
    {
        kesz[i] = s1[place];
        kesz[i + 1] = s2[place];
        place++;
    }

    kesz[osszefesul_hossz] = '\0';

    puts(kesz);
}

int main(int argc, string argv[])
{
    if(argc != 3)
    {
        puts("Hiba! Adjon meg pontosan ket sztringet! ");
    }
    else if(strlen(argv[1]) != strlen(argv[2]))
    {
        puts("Hiba! A ket sztringnek ugyanolyan hosszunak kell lennie! ");
    }
    else
    {
        merge(argv[1], argv[2]);
    }

    return 0;
}