#include <stdio.h>
#include "prog1.h"
#include <string.h>

int hamming_distance( string s1, string s2 )
{
    if(strlen(s1) != strlen(s2))
    {
        return -1;
    }
    else
    {
        int length = strlen(s1);
        int hamming = 0;
        for(int i = 0; i < length; i++)
        {
            if(s1[i] != s2[i])
            {
                hamming++;
            }
        }
        return hamming;
    }
}

int main()
{
    string szo1 = get_string("Text 1: ");
    string szo2 = get_string("Text 2: ");

    printf("A(z) '%s' es '%s' Hamming-tavolsaga: %d \n", szo1, szo2, hamming_distance(szo1, szo2));

    return 0;
}