#include <stdio.h>
#include <stdlib.h>
#define SIZE 1000

//random szamot general
int randint(int also, int felso)
{
    int veletlen = rand();
    int intervallum = felso - also + 1;

    veletlen = veletlen % intervallum;
    veletlen = also + veletlen;

    return veletlen;
}

//atlagot szamol
int avg(const int n, int tomb[])
{
    int sum = 0;

    for(int i = 0; i < n; i++)
    {
        sum += tomb[i];
    }
    return sum/n;
}

//megszamolja a nagyobbakat az atlagnal
int nagyobb(const int n, const int atlag, int tomb[])
{
    int sum = 0;

    for(int i = 0; i < n; i++)
    {
        if(tomb[i] > atlag)
        {
            sum++;
        }
    }
    printf("%d \n", sum);
}

//feltolti a tombot
void feltolt(const int n, int tomb[])
{
    srand(2021); //itt inicializaljuk a random szam generatort

    for(int i = 0; i < n; i++)
    {
        tomb[i] = randint(125, 849);
    }
}

int main()
{
    int szamok[SIZE] = {0};
    feltolt(SIZE, szamok);
    nagyobb(SIZE, avg(SIZE, szamok), szamok);

    return 0;
}