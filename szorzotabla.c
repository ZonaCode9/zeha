#include <stdio.h>

int is_prime( int n)
{
    if (n <= 1)
    {
        return 0;
    }
    if(n == 2)
    {
        return 1;
    }
    for(int i = 2; i * i <= n; i++)
    {
        if(n % i == 0)
        {
            return 0;
        }
    }
    return 1;
}

void szorzotabla(int n)
{

    for(int i = 1; i <= 10; i++)
    {
        printf("%d x %d = %d \n", n, i, n * i);
    }

}

int main()
{
    int szam = 0;

    puts("Adj meg egy primszamot!");
    scanf("%d", &szam);

    while(!is_prime(szam))
    {
        puts("Ez nem primszam, probajd ujra!");
        scanf("%d", &szam);
    }

    szorzotabla(szam);

}