#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int randint(int also, int felso)
{
    int veletlen = rand();
    int intervallum = felso - also + 1;

    veletlen = veletlen % intervallum;
    veletlen = also + veletlen;

    return veletlen;
}

int ora_generator()
{
    srand(time(NULL));
    randint(0, 23);
}

int perc_generator()
{
    srand(time(NULL));
    randint(0, 59);
}

void kiir(int ora, int perc)
{
    printf("%02d:%02d ", ora, perc);

    if(ora == 0 && perc == 0)
    {
        puts("(ejfel)");
    }
    else if(ora >= 0 && ora < 12)
    {
        puts("(delelott)");
    }
    else if(ora == 12 && perc == 0)
    {
        puts("(del)");
    }
    else
    {
        puts("(delutan)");
    }
}

int main()
{
    kiir(ora_generator(), perc_generator());

    return 0;
}